# **Bindead** - a static analysis tool for binaries.

Bindead is an analyzer for executable machine code. It features a disassembler that translates machine code bits into an assembler like language (RREIL) that in turn is then analyzed by the static analysis component using abstract interpretation. As Bindead operates on the machine code level, it can be used without having the source code of the program to be analyzed. In fact, the purpose of Bindead is to help with the reverse engineering of executable code or binaries. The analyzer enables one to reason about all the possible runtime behavior of a program and find potential bugs. To this end we perform a collection of (numeric and symbolic) analyses on the program and are able to infer memory access bounds and various other numeric properties statically, i.e. without running the program.

# Components of the analyzer:

## Disassembler

The disassembler part is designed around the idea of a modular front-end that produces RREIL code from binary code blobs. It uses pluggable front-ends for the input data (file parsing) and the disassemblers/translators. 

**Binary file formats**:
We are able to parse and extract code sections from Linux (ELF) and Windows (PE) executables and our own


Ace Pokies casino is famous for being the best Australian online casino that offers hundreds of top online pokies and many other very exciting online casino games. Australian gamblers can play the best casino games including online slots, Blackjack, Roulette, Baccarat, Video Poker, and much more in both real money and free-play mode.

Playing Australian online pokies has never been easier. To enjoy the best pokies online, all a player has to do is to visit the website directly from their browser without the need to download the software. We guarantee you that you will have the best gambling experience ever.

Ace Pokies online casino has its players' best interests at heart when it comes to the best online pokies. This means that our primary goal is to aim for the players' choice, value and unconditional support from the instant they start paying the best Australian pokies on offer.

By joining Ace Pokies online casino, you are assured to always find top online pokies real money games that will surely tickle your fancy. These include Avalon, Mega Moolah, Thunderstruck and much more. So read on and learn more about the top recommended online pokies in Australia.

WHAT MAKES POKIESWAY THE BEST ONLINE CASINO
##Top Selection of Online Pokies

We have created an extensive selection of the best available online pokies games, with currently over 1000 pokies available.

There is something to suit everyone at PokiesWay so waste no time and get ready for an unparalleled experience that you will always enjoy!
##Best Casino Promotions

At PokiesWay we love to indulge players with tons of bonuses and special promotions.

We have put together an unmatched new player welcome package, designed just like we would’ve wanted it ourselves. We think you’ll absolutely love it!
Wide selection of payment options

PokiesWay warrant to always process your payments safely and securely. We have joined forces with all major payment methods to allow you freedom of choice.

And that’s not all -we're fast too! we aim to process your withdrawals within 24 hours.

RREIL assembler text files. The input file format for binary code chunks though is built around a simple interface and plugins can easily be added for any other file format. Basically any collection of chunks of bytes can be fed to the analyzer if wrapped in our simplified binary file format.

##Play the Best Online Pokies in New Zealand – Top Pokie Sites For 2019##:
To analyze the code we currently use a hand-written disassembler that translates the binary representation of machine code (chunks of bytes) into the intermediate language RREIL that is then passed on to the analyzer. No one can deny the huge popularity of online slots among Kiwi gamblers. Online gambling enjoys several attractive features that capture the minds and hearts of all types of gamblers. There are no challenges or complications associated with registering an online account for real money gambling. We can thus use different disassembler front-ends for different architectures with the same code analyzer. Currently we have disassemblers for Intel x86-32 and x86-64 and for the AVR-8 architecture.  
As writing front-ends by hand proved to be tedious and error prone and after discussing the issue with people from the reverse engineering community the consensus was that there should be a simpler way to build and share descriptions for machine instructions. Hence, the *Generic Decoder Specification Language Toolkit* Pokies are games of chance, consisting of reels of symbols – usually either three or five reels – which Australian players must match in order to win. Classic pokies featured either one or three lines, where the player was required to match symbols to win. But today, Australia online pokies have up to 50 multiple paylines, as well as even more modern games that scrap the traditional payline aspect. These games use ‘winning ways’ – up to a potential 3,125 different combinations in which the player can win.

In real money pokies, players have a wide range they can wager on each spin. There are 1 cent, 2 cent, 25 cent and dollar real pokies, as well as plenty more other denominations people can play. With online Australian pokies, a single game can be played at a variety of stakes, allowing the player to customize the game to their liking.

There are two other ways to change the amount wagered on each spin. First, the player can choose to play each spin for a certain number of coins. This is usually anywhere between one and five coins, depending on how much it costs to play. In addition, the player can choose to play anywhere between one line and the maximum number of lines. Each line will be played for the amount of coins selected. In total, a spin on a poker machine can cost much more than the listed denomination. The total amount of each spin is the denomination multiplied by the number of coins and lines being played. The benefit of playing the maximum coins and lines is not just the higher payouts, but this is also how players win the largest progressive jackpots.

Both live and Australia online pokies use Random Number Generators (RNG) to determine the spin of each reel. This guarantees fair results, which (in the long run) will provide returns to the player near the level set by the casino. However, it also means winning or losing is a matter of pure luck. There is no skill involved when playing real pokies. They’re attractive for players who simply want to enjoy a game and have an equal chance of winning than those who play regularly.

Some Australian online pokies include bonus rounds in which the player can win large payouts after matching special bonus symbols. In these modes, players often get free spins and win at higher rates (or for higher amounts) than in the normal course of play. In some cases, these bonus rounds may even have a light skill element involved, although this is rare. Even in those cases, players generally have only a modest amount of control over their winnings. [NZ casino https://casinofisher.com/][gdsl] was started. The project
allows to specify the syntax and semantics of machine instructions in a domain specific language that was designed for exactly this purpose. GDSL compiles the instructions specification using one of the available language binding back-ends to a disassembler library that can be plugged into other projects. Currently GDSL comes with disassembler specifications for the Intel x86-32 and x86-64 and the AVR-8 architecture, thus replacing the hand written front-ends of the Bindead analyzer. 

**Disassembling process**:
The static code analyzer together with the disassembler front-ends comprise a recursive descent disassembler. It reconstructs the control flow graph (CFG) of the code while disassembling and analyzing the binary. Contrary to most common disassemblers (e.g. [`objdump`](http://en.wikipedia.org/wiki/Objdump)) that perform a linear sweep over the code, because of the recursive disassembly method and our static analysis we are able to follow the control flow of the code (e.g. indirect jumps and calls). Thus our disassembler usually produces less disassembly errors (wrongly decoded instructions), discovers more code (code vs. data problem) and is more robust when dealing with obfuscated code. However, an in depth comparison with existing disassemblers given all these problem cases has not been performed. The above statement is thus about the theoretical implications of our disassembler architecture and it depends on the performance and precision of the static analyzer component.

## Intermediate Language (RREIL)

The static analyzer uses an intermediate language that was designed to be minimal, concise and
suitable for numeric analyses. The language is inspired by the ideas that went into [Zynamics' REIL][reil] adding revisions for some shortcomings of REIL. For example by exposing1
comparisons explicitly the memory bounds analysis can be greatly improved. Hence the name **RREIL** for *Relational REIL* where **REIL** is an acronym for *Reverse Engineering Intermediate Language*. 
More information on the features of the language and its semantics can be found on the [RREIL introduction page](Introduction to RREIL) and in the [PhD Thesis][phd-thesis] about the analyzer or the [WCRE 11 paper][rreil].

In addition to the disassembler front-ends that translate binary code to RREIL we also have an assembler to RREIL. This allows us to write test examples for the analyzer directly in RREIL without the necessity to first compile code and disassemble it.

## Abstract Domains

The main part of the analyzer is the static analysis part that employs abstract interpretation with a stack of so-called abstract domains (numeric and symbolic) to infer program properties and variable values for each program point. The diagram below gives an overview of the analyzer with an emphasis on the stack of abstract domains. The main point is that the domains operate on an input language and the language becomes simpler for the bottom of the domains stack, e.g. pointers and variable sizes do not exist at the lowest level anymore. The modular design makes it fairly easy to try out new analyses due to the simplification of concerns mentioned above. As the interfaces between the domains are only defined by the input language it is easy to plug in new domains that solve a special and constrained problem.  

A short description of the abstract domains that we use can be found on [this page][domain-hierarchy-wiki]. It details the
motivation and use case of each domain. A more detailed description of the domains and how they interoperate can be found in
the [PhD Thesis][phd-thesis] about the analyzer. The thesis and the conference publications 
(in the publications section at the bottom) additionally show the problems we faced when analyzing machine code and how 
we solved these problems using novel abstract domains.

[![Domain Hierarchy](/mihaila/bindead/wiki/images/domain-hierarchy-small.png "Domain Hierarchy - click for bigger size")](/mihaila/bindead/wiki/images/domain-hierarchy.png) 


## Dynamic + Static Analysis

As an extension to the static analyzer we built a component that runs a program and outputs a trace of the run
that can then be fed to the analyzer and the abstract domains. The idea behind it is to have a dynamically started static analysis, i.e. bootstrap a static analysis at a certain point in the program with the dynamic state of the program. Additionally, the user can choose which parts of the dynamic program state should not be copied over to the static analyzer. The tracer tool BinTrace is described in more detail on its [project page][bintrace].

### Building and Installation
Please see the [README](https://bitbucket.org/mihaila/bindead/src/master/README.md) file in the source directory.

### Usage
After installation of the standalone package the analyzer can be used from the command line. Use `--help` to see possible commands or read a more detailed description on [this separate CLI section](Command Line Interface).

Invoked from the command line Bindead currently prints the disassembled code in Intel syntax and in a similar format to `objdump` plus some analysis summary information, e.g.:

~~~
#!text
Analyzed native code (25 instructions):

f:
080483ec:  8b 44 24 04             mov    eax, DWORD PTR [esp+0x4]
080483f0:  c3                      ret    

a:
080483f1:  83 ec 04                sub    esp, 0x4
080483f4:  8b 44 24 08             mov    eax, DWORD PTR [esp+0x8]
080483f8:  89 04 24                mov    DWORD PTR [esp], eax
080483fb:  e8 ec ff ff ff          call   0x80483ec <f>
08048400:  8b 44 24 08             mov    eax, DWORD PTR [esp+0x8]
08048404:  83 c4 04                add    esp, 0x4
08048407:  c3                      ret    

b:
08048408:  83 ec 04                sub    esp, 0x4
0804840b:  8b 44 24 08             mov    eax, DWORD PTR [esp+0x8]
0804840f:  89 04 24                mov    DWORD PTR [esp], eax
08048412:  e8 d5 ff ff ff          call   0x80483ec <f>
08048417:  8b 44 24 08             mov    eax, DWORD PTR [esp+0x8]
0804841b:  83 c4 04                add    esp, 0x4
0804841e:  c3                      ret    

main:
0804841f:  83 ec 14                sub    esp, 0x14
08048422:  c7 44 24 10 00 00 00 00 mov    DWORD PTR [esp+0x10], 0x0
0804842a:  c7 04 24 00 00 00 00    mov    DWORD PTR [esp], 0x0
08048431:  e8 bb ff ff ff          call   0x80483f1 <a>
08048436:  c7 04 24 01 00 00 00    mov    DWORD PTR [esp], 0x1
0804843d:  e8 c6 ff ff ff          call   0x8048408 <b>
08048442:  8b 44 24 10             mov    eax, DWORD PTR [esp+0x10]
08048446:  83 c4 14                add    esp, 0x14
08048449:  c3                      ret    

Analysis steps: 143
Warnings: 0

Nеw Саѕіnоѕ 2020



Wеlсоmе, 2020! Whаt аn іnсrеdіblе уеаr іѕ аhеаd оf uѕ, wіth рlеntу оf nеw саѕіnоѕ wаіtіng tо bе lаunсhеd аnd аmаzіng dеvеlорmеntѕ аlоng thе wау. Саѕіnоѕ аrе рорріng оut lеft аnd rіght, аnd wе rесеіvе nеw саѕіnо аnnоunсеmеntѕ аll thе tіmе. Оur tеаm оf ехреrtѕ саrеfullу еvаluаtеѕ еасh саѕіnо аnd ѕеlесtѕ thе nеw саѕіnоѕ thаt аrе thе bеѕt оn thе mаrkеt fоr еасh mоnth. Јuѕt аѕ wе dіd іn thе раѕt уеаr, wе wіll bе lіѕtіng аll bеѕt nеw саѕіnоѕ аnd саѕіnо bоnuѕеѕ mоnthlу thrоughоut 2020. Маkе ѕurе tо сhесk оur раgеѕ оn thе 1ѕt оf еасh mоnth tо lеаrn whісh nеwlу lаunсhеd саѕіnоѕ mаdе thе mоnth’ѕ lіѕt!



Wе аrе dеdісаtеd tо рrоvіdіng уоu wіth thе bеѕt саѕіnо оffеrѕ оut thеrе, іnсludіng nеw саѕіnоѕ, nеw bоnuѕеѕ, аnd ѕроrtѕ bеttіng bооkmаkеrѕ, аll оf whісh аnаlуzеd іn dеtаіl.  Тhеrе аrе рlеntу оf рауmеnt mеthоdѕ, уоu саn сhооѕе frоm, аnd wе аlwауѕ mаkе ѕurе tо сhесk саrеfullу thе tеrmѕ аnd соndіtіоnѕ, whісh wе аdvіѕе уоu tо сhесk аѕ wеll! Сlеаrlу, сuѕtоmеr ѕеrvісе іѕ vеrу іmроrtаnt fоr thе рlауеr, аnd wе mаkе ѕurе thаt thе саѕіnоѕ lіѕtеd оn оur wеbѕіtе dо hаvе рrоfеѕѕіоnаl аnd frіеndlу сuѕtоmеr ѕеrvісе. Wіth аll оf thаt bеіng ѕаіd, lеt’ѕ hаvе а lооk аt thе Glоbаl Саѕіnо аnd Gаmіng Маrkеt Тrеndѕ іn 2020!

~~~

The same example showing the complete and more verbose analysis trace output (i.e. with the RREIL code and showing register and memory values) can be seen separately on [this page](Analysis Output from Command Line).

As can be seen on the above page the trace of the analysis is very verbose, huge and difficult to understand for bigger binaries. Thus, to more conveniently view the inferred values for each program point we built a [graphical user interface (GUI)][p9] for the analyzer, named **p9**. It is able to visualize and navigate the CFG of an analyzed executable and show the values inferred by the analyzer at each program point (see the picture below).

[![GUI Screenshot](/mihaila/bindead/wiki/images/p9-screenshot-2-small.png "GUI Screenshot - click for bigger size")](/mihaila/bindead/wiki/images/p9-screenshot-2.png)  

Further details about the GUI, its features and installation instructions are on its [project page][p9]. 

## Publications
Conference publications/talks regarding Bindead or affiliated projects:

* PhD Thesis 15 - [Adaptable Static Analysis of Executables for proving the Absence of Vulnerabilities][phd-thesis]
* HES 15 - [Sendmail Crackaddr Talk at "Hackito Ergo Sum"][crackaddr-talk-hes] 
* NFM 14 - [Synthesizing Predicates from Abstract Domain Losses][predicates] - ([slides of talk][predicates-talk])
* NFM 13 - [Widening as Abstract Domain][widening] - ([slides of talk][widening-talk])
* APLAS 13 - [The Undefined Domain: Precise Relational Information for Entities that Do Not Exist][undef] - ([slides of talk][undef-talk])
* APLAS 13 - [GDSL: A Universal Toolkit for Giving Semantics to Machine Language][gdslpub2]
* TAPAS 12 - [GDSL: A Generic Decoder Specification Language for Interpreting Machine Language][gdslpub1]
* WCRE 11 - [Precise Static Analysis of Binaries by Extracting Relational Information][rreil] - ([slides of talk][rreil-talk])

## Resources
Material mentioned in the text and some additional material:

* [online casinos][gdsl]



[domain-hierarchy-wiki]: https://bitbucket.org/simona/bindead-monolith/wiki/Home "Domain Hierarchy"
[gdsl]: https://casinofisher.com/ "New casinos NZ"

